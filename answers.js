function parseMoves(moves){
  let moves_arr = [];
  const moveList = ["<",">","^","v"];

  for (let i = 0; i < moves.length; i++) {
    let m = moves[i];
    if (moveList.includes(m)){
      moves_arr.push(m);
    }
  }
  return moves_arr;
}

function addVistedLocation(move,coord){
  let lastMove = JSON.parse(JSON.stringify(coord));
  let x;
  let y;
  let nextMove;

  switch (move) {
    case "<":
      x = lastMove[0] - 1;
      y = lastMove[1];
      break;
    case ">":
      x = lastMove[0] + 1;
      y = lastMove[1];
      break;
    case "^":
      x = lastMove[0];
      y = lastMove[1] + 1;
      break;
    case "v":
      x = lastMove[0];
      y = lastMove[1] - 1;
      break;
    default:
      console.log("Invalid Move Detected");
      break;
  }
  nextMove = [x, y];
  return nextMove;
}

function mariasDeliveries(moves) {
  let start = [0,0];
  let visitedObj = {};
  let moves_arr = parseMoves(moves);
  let prev;
  visitedObj[start] = 1;

  moves_arr.forEach((mm,i)=>{
    if (i === 0) prev = start;
    prev = addVistedLocation(mm, prev);
    visitedObj[prev] ? visitedObj[prev] += 1 : visitedObj[prev] = 1;
  });

  return Object.keys(visitedObj).length;
}

function mariasAndColvisDeliveries(moves){
  let start = [0,0];
  let visitedObj = {};
  let moves_arr = parseMoves(moves);
  let mariasMoves =[];
  let clovisMoves =[];
  let prev;

  visitedObj[start] = 2;

  while (moves_arr.length > 0) {
    let mm = moves_arr.shift();
    let cm = moves_arr.shift();
    if (mm) mariasMoves.push(mm);
    if (cm) clovisMoves.push(cm);
  }

  mariasMoves.forEach((mm,i)=>{
    if (i === 0) prev = start;
    prev = addVistedLocation(mm, prev);
    visitedObj[prev] ? visitedObj[prev] += 1 : visitedObj[prev] = 1;
  });

  clovisMoves.forEach((cm,i)=>{
    if (i === 0) prev = start;
    prev = addVistedLocation(cm, prev);
    visitedObj[prev] ? visitedObj[prev] += 1 : visitedObj[prev] = 1;
  });

  return Object.keys(visitedObj).length;

}

//testcases//

const testcases = ()=>{
  const maraiTest = {
    ">": 2,
    "^>v<": 4,
    "^v^v^v^v^v": 2,
    "<><><>": 2,
    "^^^^ ^": 6
  };

  const mariaAndClovisTest = {
    ">": 2,
    "^v": 3,
    "^>v<": 3,
    "<><> <>": 7,
    "^v^v^v^v^v": 11
  };

console.log("mariasDeliveries:");
console.log(mariasDeliveries(">") === maraiTest[">"]);
console.log(mariasDeliveries("^>v<") === maraiTest["^>v<"]);
console.log(mariasDeliveries("^v^v^v^v^v") === maraiTest["^v^v^v^v^v"]);
console.log(mariasDeliveries("<><><>") === maraiTest["<><><>"]);
console.log(mariasDeliveries("^^^^ ^") === maraiTest["^^^^ ^"]);
console.log(mariasDeliveries("^>^v>>><^") === 8);
console.log();
console.log("mariasAndColvisDeliveries:");
console.log(mariasAndColvisDeliveries(">") === mariaAndClovisTest[">"]);
console.log(mariasAndColvisDeliveries("^v") === mariaAndClovisTest["^v"]);
console.log(mariasAndColvisDeliveries("^>v<") === mariaAndClovisTest["^>v<"]);
console.log(mariasAndColvisDeliveries("<><> <>") === mariaAndClovisTest["<><> <>"]);
console.log(mariasAndColvisDeliveries("^v^v^v^v^v") === mariaAndClovisTest["^v^v^v^v^v"]);
console.log(mariasAndColvisDeliveries("^ > ^ v > > > < ^") === 9);

};
testcases();
